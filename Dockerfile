FROM debian:buster

# Install all the extra packages we need to build anything inside the box
RUN cp /etc/apt/sources.list /etc/apt/sources.list.old \
  && echo 'deb http://deb.debian.org/debian bullseye main' >> /etc/apt/sources.list \
  && DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -yt buster \
    build-essential gfortran git lua5.3 luarocks python3 curl time xz-utils \
  && apt-get install -yt bullseye binutils \
  && apt-get clean && rm -rf /var/lib/apt/lists/* \
  && mv /etc/apt/sources.list.old /etc/apt/sources.list

# The Lua extensions are a little trickier to get our hands on
RUN luarocks install serpent \
  && git clone https://github.com/Phrogz/SLAXML.git /tmp/slaxml \
  && cp /tmp/slaxml/*.lua /usr/local/share/lua/5.3/ \
  && rm -rf /tmp/slaxml

# Clone the special Spack install, and add in our special packages.yaml
RUN git clone https://github.com/blue42u/spack.git /spack \
    && cd /spack && git checkout 6b28e82442a221c670615e1a7cd3a56e1814eaff

# Compile up the Dyninst bits we need
ARG JOBS=16
ENV D_INSTALL_DIR=/install D_SPEC="hpctoolkit@2020.08.03 ^dyninst@develop-ppopp21 ^elfutils@0.178"
RUN /spack/bin/spack install -j${JOBS} "${D_SPEC}" \
  && /spack/bin/spack clean -a \
  && mkdir -p ${D_INSTALL_DIR} \
  && /spack/bin/spack view soft -i "${D_INSTALL_DIR}" "${D_SPEC}"

# Copy in all the bits. This can take a while so we do it early
ENV CORRECTNESS_DIR=/correctness PERFORMANCE_DIR=/performance
# Large bits first
ADD binaries.tar.xz ${CORRECTNESS_DIR}/inputs/
ADD rtl.tar.xz ${CORRECTNESS_DIR}/rtl/
ADD camellia.tar.xz tensorflow.tar.xz ${PERFORMANCE_DIR}/inputs/
# Smaller bits next
ADD jtdump.lua cfgdump.cpp ${CORRECTNESS_DIR}/
COPY Makefile.correctness ${CORRECTNESS_DIR}/Makefile
ADD hpcstructprof.sh hpcdump.lua perfresults.lua ${PERFORMANCE_DIR}/
COPY Makefile.performance ${PERFORMANCE_DIR}/Makefile

# Run the correctness bits
RUN make -C ${CORRECTNESS_DIR} cfgdump
RUN make -C ${CORRECTNESS_DIR} -j${JOBS} results.diff

# Run the performance bits
#RUN make -C ${PERFORMANCE_DIR} results-camellia.txt
#RUN make -C ${PERFORMANCE_DIR} results-tensorflow.txt
