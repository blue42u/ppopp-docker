#!/bin/bash

# Settings
ulimit -c 0
declare -A LIBS
LIBS[lib/hpctoolkit/libhpcrun.so]=libhpcrun
LIBS[lib/hpctoolkit/ext-libs/libmonitor.so]=libmonitor
LIBS[lib/libtbbmalloc_proxy.so.2]=libtbbmalloc_proxy
LIBS[lib/libtbbmalloc.so.2]=libtbbmalloc
LIBS[libexec/hpctoolkit/hpcstruct-bin]=hpcstruct-bin
LIBS[lib/libparseAPI.so]=libparseAPI
LIBS[lib/libinstructionAPI.so]=libinstructionAPI
LIBS[lib/libsymtabAPI.so]=libsymtabAPI
LIBS[lib/libdynDwarf.so]=libdynDwarf
LIBS[lib/libdynElf.so]=libdynElf
LIBS[lib/libcommon.so]=libcommon
LIBS[lib/libboost_filesystem.so]=libboost_filesystem
LIBS[lib/libdw.so]=libdw
LIBS[lib/libtbb.so.2]=libtbb

# Setup so we can move a little easier
set -e
HERE="`realpath "$(dirname "$0")"`"

# Parse the arguments.
usage() {
  cat >&2 <<EOF
Usage: $0 [<options>] [--] <binary>

    -j <threads>      Set hpcstruct threads to use for trace. (Default: 1)
    -e <event>        Set events for hpcrun. (Default: REALTIME@1000)
                      Can be given multiple times for multiple events.
    -o <output>       Set output directory name
    -S <structfile>   Add a structfile to speed up hpcprof processing.
                      Can be given multiple times for multiple structfiles.
    -s                Automatically build missing temporary structfiles for
                      known binaries.
    -C <dir>          Cache automatically built structfiles in the given
                      directory.
    -c                Shorthand for -C "$HERE/structcache".
EOF
  exit 1;
}
STRUCTFILES=()
STRUCTEXPAND=""
STRUCTFLAGS=('-o' '/dev/null')
RUNFLAGS=('-t')
PROFFLAGS=()
THREADS=""
EVENT=""
DBOUT=""
CACHEDIR=""
while getopts "e:o:j:S:sC:c" o; do
  case "${o}" in
    e) RUNFLAGS+=('-o' "$OPTARG"); EVENT='y' ;;
    o) if [ "$DBOUT" ]; then usage; else DBOUT="$OPTARG"; fi ;;
    j) if [ "$THREADS" ]; then usage; else THREADS="$OPTARG"; fi ;;
    S) PROFFLAGS+=('-S' "$OPTARG"); STRUCTFILES+=("$OPTARG") ;;
    s) STRUCTEXPAND="1" ;;
    C) if [ "$CACHEDIR" ]; then usage; else CACHEDIR="$OPTARG"; fi ;;
    c) if [ "$CACHEDIR" ]; then usage; else CACHEDIR="$HERE"/structcache; fi ;;
    *) usage ;;
  esac
done
shift $((OPTIND-1))
if [ "$#" != "1" ]; then usage; fi

# Handle default options
if [ "$CACHEDIR" ] && ! [ "$STRUCTEXPAND" ]; then
  echo "Flag -(C|c) requires -s to function!" >&2
  exit 1
fi
if [ "$THREADS" ]; then
  STRUCTFLAGS+=('-j' "$THREADS" '--jobs-symtab' "$THREADS")
else THREADS=1; fi
if [ -z "$EVENT" ]; then
  echo "Defaulting to REALTIME@1000 for trace data" >&2
  RUNFLAGS+=('-e' 'REALTIME@1000')
fi
if [ -z "$DBOUT" ]; then
  DBOUT="hpctoolkit-`basename "$1"`-t$THREADS"
  echo "Defaulting output to $DBOUT" >&2
fi
PROFFLAGS+=('-o' "$DBOUT")
RUNFLAGS+=('-o' "$DBOUT"-hpcrun)

# Make a temporary directory for our bits
trap 'rm -fr "$TMP"' EXIT
TMP="`mktemp -d`"

# Do the actual run
rm -rf "$DBOUT"-hpcrun
echo "Running hpcrun..." >&2
LD_PRELOAD="${D_INSTALL_DIR}"/lib/libtbbmalloc_proxy.so.2 \
\time "${D_INSTALL_DIR}"/bin/hpcrun \
  "${RUNFLAGS[@]}" "${D_INSTALL_DIR}"/bin/hpcstruct "${STRUCTFLAGS[@]}" "$1"

expand () {
  # First scan the -S structs for an overriding structfile
  if [ "${#STRUCTFILES[@]}" -gt 0 ] &&  grep '<LM.*n="'"$lm"'"' "${STRUCTFILES[@]}" &>/dev/null; then
    echo "Found explicit struct for $stem." >&2
    return
  fi

  if [ "$CACHEDIR" ]; then
    mkdir -p "$CACHEDIR"
    # Check the cache for a proper structfile.
    output="$CACHEDIR"/"$stem"."$(md5sum "$lm" | cut -d' ' -f1)".hpcstruct
    if [ -f "$output" ]; then
      echo "Found cached struct for $stem." >&2
      PROFFLAGS+=('-S' "$output")
      return
    fi
  else
    # Just use a spot in the temporary location.
    output="$TMP"/"$stem".hpcstruct
    echo "No structfile for $lm!" >&2
  fi

  # If we have to build a structfile, we will.
  echo "Building structfile for $stem..." >&2
  if "${D_INSTALL_DIR}"/bin/hpcstruct -o "$output" "$lm"; then
    PROFFLAGS+=('-S' "$output")
  else
    echo "Failed to build, skipping..." >&2
    rm "$output"
  fi
}

# If requested, we'll write structfiles for every path we don't have yet.
if [ "$STRUCTEXPAND" ]; then
  for f in "${!LIBS[@]}"; do
    lm="`realpath "${D_INSTALL_DIR}"/$f`"
    stem="${LIBS[$f]}"
    expand "$lm" "$stem"
  done
fi

# Run HPCProf on the output
rm -rf "$DBOUT"
echo "Running hpcprof..." >&2
"${D_INSTALL_DIR}"/bin/hpcprof "${PROFFLAGS[@]}" "$DBOUT"-hpcrun
