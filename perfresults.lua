-- luacheck: lua53

serpent = require 'serpent'

local function dload(fn)
  local f = io.open(fn, 'r')
  local data = f:read 'a'
  f:close()
  local _
  _,data = assert(serpent.load(data))
  return data
end
local name = arg[1]
local data = {}
assert(#arg % 2 == 1)
for i=2,#arg,2 do
  local t,fn = tonumber(arg[i]), arg[i+1]
  assert(not data[t])
  data[t] = dload(fn)
end

-- Make sure we process in increasing order of threads
local threads = {}
for t in pairs(data) do table.insert(threads, t) end
table.sort(threads)
local mt = threads[#threads]

print(name)
print(('%5s %7s  %5s  %6s %16s'):format('Cores', 'Prior', '%SCFG', 'CFG', 'Current'))
for _,t in pairs(threads) do
  local d = data[t]
  local prior = d.exec[1].avg - d.parsePF[1].avg + data[1].parsePF[1].avg
  print(('%5d %7.2f  %5.2f%% %6.2f %7.2f +- %5.2f'):format(
    t, prior, data[1].parsePF[1].avg / prior * 100, d.parsePF[1].avg,
    d.exec[1].avg, d.exec[1].sd))
  if t > 1 then
    print(('%5s   %5.2fx %6s  %5.2fx  %5.2fx'):format(
      '',
      data[1].exec[1].avg / prior,
      '',
      data[1].parsePF[1].avg / d.parsePF[1].avg,
      data[1].exec[1].avg / d.exec[1].avg))
  end
end
